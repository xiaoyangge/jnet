package com.jfireframework.jnet.common.buffer;

public enum SizeType
{
	TINY, SMALL, NORMAL
}
